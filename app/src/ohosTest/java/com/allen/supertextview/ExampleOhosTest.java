/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.allen.supertextview;

import com.allen.library.BaseTextView;
import com.allen.library.SuperTextView;
import junit.framework.TestCase;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

import static com.allen.supertextview.EventHelper.waitForActive;

public class ExampleOhosTest extends TestCase {

    private IAbilityDelegator mIAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    private Context mContext;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mContext = mIAbilityDelegator.getAppContext();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        Thread.sleep(2000);
        EventHelper.clearAbilities();
    }

    @Test
    public void testAllCombinations() {
        SuperTextAbility0 ability = EventHelper.startAbility(SuperTextAbility0.class);
        waitForActive(ability, 2000);
        SuperTextView mTv6 = (SuperTextView) ability.findComponentById(ResourceTable.Id_tv6);
        mTv6.setLeftText("100000");
        mTv6.setCenterText("666666");
        mTv6.setRightText("888888");
        try {
            mTv6.setRightTextColor(mContext.getResourceManager().getElement(ResourceTable.Color_black).getColor());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        mTv6.setLeftImageVisibility(false);
        mTv6.setRightArrowImageVisibility(false);
        try {
            Assert.assertEquals(mTv6.getRightTextColor(), mContext.getResourceManager().getElement(ResourceTable.Color_black).getColor());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        SuperTextView mTv9 = (SuperTextView) ability.findComponentById(ResourceTable.Id_tv9);
        mTv9.setLeftText("声音");
        mTv9.setLeftImageVisibility(false);
        mTv9.setSwitchVisibility(true);
        mTv9.setSwitchIsChecked(true);
        Assert.assertTrue(mTv9.getSwitch().isChecked());
    }

    @Test
    public void testCurrency() {
        SuperTextAbility ability = EventHelper.startAbility(SuperTextAbility.class);
        waitForActive(ability, 2000);
        SuperTextView mTv3 = (SuperTextView) ability.findComponentById(ResourceTable.Id_tv3);
        mTv3.setLeftImageVisibility(false);
        mTv3.setLeftText("账号与安全");
        mTv3.setRightImage(ResourceTable.Media_ic_defend);
        Assert.assertTrue(mTv3.getRightImage().getPixelMap().getImageInfo().size.height > 0);
    }

    @Test
    public void testELeMe() {
        SuperTextAbility1 ability = EventHelper.startAbility(SuperTextAbility1.class);
        waitForActive(ability, 2000);
        SuperTextView mTv4 = (SuperTextView) ability.findComponentById(ResourceTable.Id_tv4);
        mTv4.setLeftImage(ResourceTable.Media_ic_integral);
        mTv4.setLeftText("积分商城");
        mTv4.setRightImage(ResourceTable.Media_ic_right);
        Assert.assertFalse(mTv4.getLeftText().getText().isEmpty());
    }

    @Test
    public void testFinance() {
        SuperTextAbility2 ability = EventHelper.startAbility(SuperTextAbility2.class);
        waitForActive(ability, 2000);
        SuperTextView mTv1 = (SuperTextView) ability.findComponentById(ResourceTable.Id_tv1);
        mTv1.setLeftImage(ResourceTable.Media_ic_accrual);
        mTv1.setLeftText("起息时间");
        mTv1.setRightText("立即起息");
        mTv1.setRightArrowImageVisibility(false);
        try {
            mTv1.setRightTextColor(mContext.getResourceManager().getElement(ResourceTable.Color_red).getColor());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        try {
            Assert.assertTrue(mTv1.getRightTextColor() == mContext.getResourceManager().getElement(ResourceTable.Color_red).getColor()
                    && mTv1.getLeftImage().getPixelMap().getImageInfo().size.height > 0);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testLeftCenterRight() {
        SuperTextAbility3 ability = EventHelper.startAbility(SuperTextAbility3.class);
        waitForActive(ability, 2000);
        SuperTextView mTv1 = (SuperTextView) ability.findComponentById(ResourceTable.Id_tv1);
        mTv1.setLeftImage(ResourceTable.Media_ic_accrual);
        mTv1.setLeftText("156****2525");
        mTv1.setCenterText("10:10");
        mTv1.setRightText("100元");
        mTv1.setLeftImageVisibility(false);
        mTv1.setRightArrowImageVisibility(false);
        Assert.assertEquals(mTv1.getRightText().getTextAlignment(), TextAlignment.RIGHT);
    }

    @Test
    public void testIncludeImage() {
        SuperTextAbility7 ability = EventHelper.startAbility(SuperTextAbility7.class);
        waitForActive(ability, 2000);
        BaseTextView mTv4 = (BaseTextView) ability.findComponentById(ResourceTable.Id_tv4);
        Assert.assertTrue(mTv4.getRightImage().getPixelMap().getImageInfo().size.height > 0);
    }

    @Test
    public void testAlignment() {
        SuperTextAbility8 ability = EventHelper.startAbility(SuperTextAbility8.class);
        EventHelper.waitForActive(ability, 2000);
        SuperTextView mTv1 = (SuperTextView) ability.findComponentById(ResourceTable.Id_tv1);
        mTv1.setLeftText("左上");
        mTv1.setCenterText("中上");
        mTv1.setRightText("右");
        mTv1.setLeftImageVisibility(false);
        mTv1.setRightArrowImageVisibility(false);
        Assert.assertEquals(mTv1.getCenterDependentLayout().getAlignment(), LayoutAlignment.CENTER);
    }

    @Test
    public void testSwitch() throws InterruptedException {
        SuperTextAbility4 ability = EventHelper.startAbility(SuperTextAbility4.class);
        waitForActive(ability, 2000);
        SuperTextView mTv1 = (SuperTextView) ability.findComponentById(ResourceTable.Id_tv2);
        mTv1.setLeftText("系统默认");
        mTv1.setLeftImageVisibility(false);
        mTv1.setSwitchVisibility(true);
        EventHelper.triggerClickEvent(ability, mTv1.getSwitch());
        Thread.sleep(2000L);
        Assert.assertTrue(mTv1.getSwitch().isChecked());
    }

    @Test
    public void testShape() {
        SuperTextAbility9 ability = EventHelper.startAbility(SuperTextAbility9.class);
        waitForActive(ability, 2000);
        SuperTextView mTv3 = (SuperTextView) ability.findComponentById(ResourceTable.Id_tv3);
        mTv3.setLeftText("上圆角");
        mTv3.setLeftImageVisibility(false);
        mTv3.setLineVisibility(false);
        ShapeElement element = new ShapeElement(mContext, ResourceTable.Graphic_background_supertext_red);
        element.setCornerRadiiArray(new float[]{
                20f, 20f,
                20f, 20f,
                0f, 0f,
                0f, 0f
        });
        mTv3.setBackground(element);
        Assert.assertSame(mTv3.getBackgroundElement(), element);
    }

    @Test
    public void testInListContainer() {
        SuperTextAbility10 ability = EventHelper.startAbility(SuperTextAbility10.class);
        waitForActive(ability, 2000);
        ListContainer listContainer = (ListContainer) ability.findComponentById(ResourceTable.Id_listContainer);
        Component item = listContainer.getComponentAt(0);
        BaseTextView baseTextView = (BaseTextView) item.findComponentById(ResourceTable.Id_baseTextView);
        Assert.assertEquals(baseTextView.getVisibility(), Component.VISIBLE);
    }

    @Test
    public void testClick() throws InterruptedException {
        SuperTextAbility11 ability = EventHelper.startAbility(SuperTextAbility11.class);
        waitForActive(ability, 2000);
        SuperTextView mTv9 = (SuperTextView) ability.findComponentById(ResourceTable.Id_tv9);
        mTv9.setLeftText("声音");
        mTv9.setLeftImageVisibility(false);
        mTv9.setSwitchVisibility(true);
        mTv9.setSwitchIsChecked(true);
        EventHelper.triggerClickEvent(ability, mTv9.getSwitch());
        Thread.sleep(2000L);
        Assert.assertFalse(mTv9.getSwitch().isChecked());
    }

}