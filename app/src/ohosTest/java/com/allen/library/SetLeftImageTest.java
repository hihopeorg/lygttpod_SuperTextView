/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.allen.library;

import com.allen.supertextview.EventHelper;
import com.allen.supertextview.ResourceTable;
import com.allen.supertextview.SuperTextAbility;
import org.junit.Assert;
import org.junit.Test;

import static com.allen.supertextview.EventHelper.waitForActive;

public class SetLeftImageTest {

    @Test
    public void testSetLeftImage() {
        SuperTextAbility ability = EventHelper.startAbility(SuperTextAbility.class);
        waitForActive(ability, 2000);
        SuperTextView mTv1 = (SuperTextView) ability.findComponentById(ResourceTable.Id_tv1);
        mTv1.setLeftImage(ResourceTable.Media_ic_head);
        Assert.assertTrue(mTv1.getLeftImage().getPixelMap().getImageInfo().size.height > 0);
    }
}