/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.allen.library;

import com.allen.supertextview.EventHelper;
import com.allen.supertextview.ResourceTable;
import com.allen.supertextview.SuperTextAbility13;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.agp.components.Component;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

import static com.allen.supertextview.EventHelper.waitForActive;

public class SetLineVisibilityTest {

    @Test
    public void testSetLineVisibility() {
        IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        Context context = sAbilityDelegator.getAppContext();
        SuperTextAbility13 ability = EventHelper.startAbility(SuperTextAbility13.class);
        waitForActive(ability, 2000);
        SuperTextView mTv2 = (SuperTextView) ability.findComponentById(ResourceTable.Id_tv2);
        mTv2.setCenterText("上下渐变色");
        try {
            mTv2.setCenterTextColor(context.getResourceManager().getElement(ResourceTable.Color_white).getColor());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        mTv2.setLeftImageVisibility(false);
        mTv2.setRightArrowImageVisibility(false);
        mTv2.setLineVisibility(false);
        Assert.assertEquals(mTv2.getLineComponent().getVisibility(), Component.HIDE);
    }

}