/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.allen.library;

import com.allen.supertextview.EventHelper;
import com.allen.supertextview.ResourceTable;
import com.allen.supertextview.SuperTextAbility8;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

import static com.allen.supertextview.EventHelper.waitForActive;


public class SetCenterTextColorTest {

    @Test
    public void testSetCenterTextColor() throws NotExistException, WrongTypeException, IOException {
        IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
        Context context = sAbilityDelegator.getAppContext();
        SuperTextAbility8 ability = EventHelper.startAbility(SuperTextAbility8.class);
        waitForActive(ability, 2000);

        SuperTextView mTv1 = (SuperTextView) ability.findComponentById(ResourceTable.Id_tv1);
        mTv1.setLeftText("左上");
        mTv1.setCenterText("中上");
        mTv1.setRightText("右");
        mTv1.setCenterTextColor(context.getResourceManager().getElement(ResourceTable.Color_c_FF4081).getColor());
        mTv1.setLeftImageVisibility(false);
        mTv1.setRightArrowImageVisibility(false);
        Assert.assertEquals(mTv1.getCenterText().getTextColor().getValue(), context.getResourceManager().getElement(ResourceTable.Color_c_FF4081).getColor());
    }

}