/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.allen.supertextview;

import ohos.agp.components.AttrSet;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.app.Context;

public class HosTitleBar extends DependentLayout {

    private Image mBackImage;
    private Text mTitleText, mRightText;
    private Image mRightImage;
    private final int PADDING = 50;

    public HosTitleBar(Context context) {
        super(context);
        initView();
    }

    public HosTitleBar(Context context, AttrSet attrSet) {
        super(context, attrSet);
        initView();
    }

    public HosTitleBar(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initView();
    }

    private void initView() {
        setWidth(LayoutConfig.MATCH_PARENT);
        ShapeElement element = new ShapeElement(getContext(), ResourceTable.Graphic_background_ability_titlebar);
        setBackground(element);
        initBackImage();
        initTitleText();
        initRightText();
        initRightImage();
    }

    private void initBackImage() {
        DependentLayout dependentLayout = new DependentLayout(getContext());
        dependentLayout.setHeight(LayoutConfig.MATCH_PARENT);
        dependentLayout.setWidth(LayoutConfig.MATCH_CONTENT);
        dependentLayout.setAlignment(LayoutAlignment.VERTICAL_CENTER);
        dependentLayout.setPaddingLeft(PADDING);
        mBackImage = new Image(getContext());
        mBackImage.setPixelMap(ResourceTable.Media_ic_back);
        dependentLayout.addComponent(mBackImage);
        addComponent(dependentLayout);
    }

    private void initTitleText() {
        DependentLayout dependentLayout = new DependentLayout(getContext());
        dependentLayout.setHeight(LayoutConfig.MATCH_PARENT);
        dependentLayout.setWidth(LayoutConfig.MATCH_PARENT);
        dependentLayout.setAlignment(LayoutAlignment.CENTER);
        mTitleText = new Text(getContext());
        mTitleText.setTextColor(Color.WHITE);
        mTitleText.setTextSize(20, Text.TextSizeType.FP);
        dependentLayout.addComponent(mTitleText);
        addComponent(dependentLayout);
    }

    private void initRightText() {
        DependentLayout dependentLayout = new DependentLayout(getContext());
        dependentLayout.setHeight(LayoutConfig.MATCH_PARENT);
        dependentLayout.setWidth(LayoutConfig.MATCH_PARENT);
        dependentLayout.setAlignment(LayoutAlignment.RIGHT | LayoutAlignment.VERTICAL_CENTER);
        dependentLayout.setPaddingRight(PADDING);
        mRightText = new Text(getContext());
        mRightText.setTextColor(Color.WHITE);
        mRightText.setTextSize(16, Text.TextSizeType.FP);
        dependentLayout.addComponent(mRightText);
        addComponent(dependentLayout);
    }

    private void initRightImage() {
        DependentLayout dependentLayout = new DependentLayout(getContext());
        dependentLayout.setHeight(LayoutConfig.MATCH_PARENT);
        dependentLayout.setWidth(LayoutConfig.MATCH_PARENT);
        dependentLayout.setAlignment(LayoutAlignment.RIGHT | LayoutAlignment.VERTICAL_CENTER);
        dependentLayout.setPaddingRight(PADDING);
        mRightImage = new Image(getContext());
        dependentLayout.addComponent(mRightImage);
        addComponent(dependentLayout);
    }

    public Image getBackImage() {
        return mBackImage;
    }

    public Text getTitleText() {
        return mTitleText;
    }

    public Text getRightText() {
        return mRightText;
    }

    public Image getRightImage() {
        return mRightImage;
    }

}
