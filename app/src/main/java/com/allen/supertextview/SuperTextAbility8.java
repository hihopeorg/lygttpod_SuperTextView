package com.allen.supertextview;

import com.allen.library.SuperTextView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;
import ohos.global.resource.NotExistException;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

public class SuperTextAbility8 extends Ability {

    private HosTitleBar mTitleBar;
    private SuperTextView mTv1, mTv2, mTv3, mTv4, mTv5, mTv6;
    private int mTextColor;

    @Override
    public void onStart(Intent intent) {
        Window window = getWindow();
        window.setStatusBarColor(new Color().rgb(65, 105, 225));
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_super_text8);
        initView(intent);
        initTv1();
        initTv2();
        initTv3();
        initTv4();
        initTv5();
        initTv6();
    }

    private void initView(Intent intent) {
        mTitleBar = (HosTitleBar) findComponentById(ResourceTable.Id_titleBar);
        mTitleBar.getBackImage().setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                terminateAbility();
            }
        });
        mTitleBar.getTitleText().setText(intent.getStringParam("name"));

        ResourceManager resourceManager = getResourceManager();
        try {
            mTextColor = resourceManager.getElement(ResourceTable.Color_c_FF4081).getColor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
    }

    private void initTv1() {
        mTv1 = (SuperTextView) findComponentById(ResourceTable.Id_tv1);
        mTv1.setLeftText("左上");
        mTv1.setCenterText("中上");
        mTv1.setRightText("右");
        mTv1.setLeftTextColor(mTextColor);
        mTv1.setCenterTextColor(mTextColor);
        mTv1.setRightTextColor(mTextColor);
        mTv1.setLeftImageVisibility(false);
        mTv1.setRightArrowImageVisibility(false);
    }

    private void initTv2() {
        mTv2 = (SuperTextView) findComponentById(ResourceTable.Id_tv2);
        mTv2.setLeftText("左中文字");
        mTv2.setCenterText("中");
        mTv2.setRightText("右中");
        mTv2.setLeftTextColor(mTextColor);
        mTv2.setCenterTextColor(mTextColor);
        mTv2.setRightTextColor(mTextColor);
        mTv2.setLeftImageVisibility(false);
        mTv2.setRightArrowImageVisibility(false);
    }

    private void initTv3() {
        mTv3 = (SuperTextView) findComponentById(ResourceTable.Id_tv3);
        mTv3.setLeftText("左下");
        mTv3.setCenterText("中下");
        mTv3.setRightText("右");
        mTv3.setLeftTextColor(mTextColor);
        mTv3.setCenterTextColor(mTextColor);
        mTv3.setRightTextColor(mTextColor);
        mTv3.setLeftImageVisibility(false);
        mTv3.setRightArrowImageVisibility(false);
    }

    private void initTv4() {
        mTv4 = (SuperTextView) findComponentById(ResourceTable.Id_tv4);
        mTv4.setLeftText("张三");
        mTv4.setCenterText("测试文字");
        mTv4.setRightText("李四");
        mTv4.setLeftImageVisibility(false);
        mTv4.setRightArrowImageVisibility(false);
    }

    private void initTv5() {
        mTv5 = (SuperTextView) findComponentById(ResourceTable.Id_tv5);
        mTv5.setLeftText("18888888888");
        mTv5.setCenterText("测试文字");
        mTv5.setRightText("15555555555");
        mTv5.setLeftImageVisibility(false);
        mTv5.setRightArrowImageVisibility(false);
    }

    private void initTv6() {
        mTv6 = (SuperTextView) findComponentById(ResourceTable.Id_tv6);
        mTv6.setLeftText("领队");
        mTv6.setCenterText("张姐专业广场舞");
        mTv6.setRightText("");
        mTv6.setLeftImageVisibility(false);
        mTv6.setRightArrowImageVisibility(false);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
