/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.allen.supertextview.slice;

import com.allen.supertextview.*;
import com.allen.supertextview.bean.MainItem;
import com.allen.supertextview.provider.MainProvider;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice {

    private ListContainer mListContainer;
    private MainProvider mMainProvider;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        initView();
    }

    private void initView() {
        mListContainer = (ListContainer) findComponentById(ResourceTable.Id_listContainer);
        List<MainItem> list = new ArrayList<>();
        list.add(new MainItem("demo是最好的老师"));
        list.add(new MainItem("所有组合"));
        list.add(new MainItem("通用"));
        list.add(new MainItem("饿了么"));
        list.add(new MainItem("金融类"));
        list.add(new MainItem("左中右"));
        list.add(new MainItem("带图片类"));
        list.add(new MainItem("对齐效果"));
        list.add(new MainItem("滑动按钮"));
        list.add(new MainItem("圆角样式"));
        list.add(new MainItem("在列表中使用"));
        list.add(new MainItem("点击事件使用"));
        list.add(new MainItem("超级按钮"));
        list.add(new MainItem("Shape&Shadow"));
        list.add(new MainItem("阴影控件"));
        mMainProvider = new MainProvider(list, this);
        mListContainer.setItemProvider(mMainProvider);
        mListContainer.setItemClickedListener((listContainer, component, i, l) -> {
            if (i == 0) return;

            String abilityName = "";
            switch (i) {
                case 1:
                    abilityName = SuperTextAbility0.class.getName();
                    break;
                case 2:
                    abilityName = SuperTextAbility.class.getName();
                    break;
                case 3:
                    abilityName = SuperTextAbility1.class.getName();
                    break;
                case 4:
                    abilityName = SuperTextAbility2.class.getName();
                    break;
                case 5:
                    abilityName = SuperTextAbility3.class.getName();
                    break;
                case 6:
                    abilityName = SuperTextAbility7.class.getName();
                    break;
                case 7:
                    abilityName = SuperTextAbility8.class.getName();
                    break;
                case 8:
                    abilityName = SuperTextAbility4.class.getName();
                    break;
                case 9:
                    abilityName = SuperTextAbility9.class.getName();
                    break;
                case 10:
                    abilityName = SuperTextAbility10.class.getName();
                    break;
                case 11:
                    abilityName = SuperTextAbility11.class.getName();
                    break;
                case 12:
                    abilityName = SuperTextAbility12.class.getName();
                    break;
                case 13:
                    abilityName = SuperTextAbility13.class.getName();
                    break;
            }
            MainItem mainItem = (MainItem) mMainProvider.getItem(i);
            Intent intent = new Intent();
            intent.setParam("name", mainItem.getName());
            Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getBundleName())
                    .withAbilityName(abilityName)
                    .build();
            intent.setOperation(operation);
            startAbility(intent);
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
