package com.allen.supertextview;


import com.allen.library.SuperTextView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;

public class SuperTextAbility extends Ability implements Component.ClickedListener {

    private HosTitleBar mTitleBar;
    private SuperTextView mTv1, mTv2, mTv3, mTv4, mTv5;

    @Override
    public void onStart(Intent intent) {
        Window window = getWindow();
        window.setStatusBarColor(new Color().rgb(65, 105, 225));
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_super_text);
        initView(intent);
        initTv1();
        initTv2();
        initTv3();
        initTv4();
        initTv5();
    }

    private void initView(Intent intent) {
        mTitleBar = (HosTitleBar) findComponentById(ResourceTable.Id_titleBar);
        mTitleBar.getBackImage().setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                terminateAbility();
            }
        });
        mTitleBar.getTitleText().setText(intent.getStringParam("name"));
        mTitleBar.getRightImage().setPixelMap(ResourceTable.Media_ic_share);
    }

    private void initTv1() {
        mTv1 = (SuperTextView) findComponentById(ResourceTable.Id_tv1);
        mTv1.setLeftImage(ResourceTable.Media_ic_head);
        mTv1.setLeftText("账号管理");
    }

    private void initTv2() {
        mTv2 = (SuperTextView) findComponentById(ResourceTable.Id_tv2);
        mTv2.setLeftImageVisibility(false);
        mTv2.setLeftText("通知设置");
    }

    private void initTv3() {
        mTv3 = (SuperTextView) findComponentById(ResourceTable.Id_tv3);
        mTv3.setLeftImageVisibility(false);
        mTv3.setLeftText("账号与安全");
        mTv3.setRightText("已保护");
        mTv3.setRightImage(ResourceTable.Media_ic_defend);
    }

    private void initTv4() {
        mTv4 = (SuperTextView) findComponentById(ResourceTable.Id_tv4);
        mTv4.setLeftImageVisibility(false);
        mTv4.setLeftText("意见反馈");
    }

    private void initTv5() {
        mTv5 = (SuperTextView) findComponentById(ResourceTable.Id_tv5);
        mTv5.setLeftImageVisibility(false);
        mTv5.setLeftText("关于微博");
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {

    }
}
