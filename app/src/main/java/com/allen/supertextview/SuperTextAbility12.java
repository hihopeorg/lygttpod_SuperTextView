package com.allen.supertextview;


import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;

public class SuperTextAbility12 extends Ability implements Component.ClickedListener {

    private HosTitleBar mTitleBar;
    private Button mBtn, mBtn2;

    @Override
    public void onStart(Intent intent) {
        Window window = getWindow();
        window.setStatusBarColor(new Color().rgb(65, 105, 225));
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_super_text12);
        initView(intent);
    }

    private void initView(Intent intent) {
        mTitleBar = (HosTitleBar) findComponentById(ResourceTable.Id_titleBar);
        mTitleBar.getBackImage().setClickedListener(component -> terminateAbility());
        mTitleBar.getTitleText().setText(intent.getStringParam("name"));
        mBtn = (Button) findComponentById(ResourceTable.Id_btn);
        mBtn2 = (Button) findComponentById(ResourceTable.Id_btn2);
        mBtn.setClickedListener(this);
        mBtn2.setClickedListener(this);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {

        }
    }
}
