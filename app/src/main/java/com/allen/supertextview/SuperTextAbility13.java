package com.allen.supertextview;

import com.allen.library.SuperTextView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;
import ohos.global.resource.NotExistException;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

public class SuperTextAbility13 extends Ability implements Component.ClickedListener {

    private HosTitleBar mTitleBar;
    private SuperTextView mTv1, mTv2;
    private int mTextColor;

    private ResourceManager mResourceManager;

    @Override
    public void onStart(Intent intent) {
        Window window = getWindow();
        window.setStatusBarColor(new Color().rgb(65, 105, 225));
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_super_text13);
        initView(intent);
        initTv1();
        initTv2();
    }

    private void initView(Intent intent) {
        mTitleBar = (HosTitleBar) findComponentById(ResourceTable.Id_titleBar);
        mTitleBar.getBackImage().setClickedListener(component -> terminateAbility());
        mTitleBar.getTitleText().setText(intent.getStringParam("name"));

        mResourceManager = getResourceManager();
        try {
            mTextColor = mResourceManager.getElement(ResourceTable.Color_gray).getColor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
    }

    private void initTv1() {
        mTv1 = (SuperTextView) findComponentById(ResourceTable.Id_tv1);
        mTv1.setCenterText("左右渐变色");
        try {
            mTv1.setCenterTextColor(mResourceManager.getElement(ResourceTable.Color_white).getColor());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        mTv1.setLeftImageVisibility(false);
        mTv1.setRightArrowImageVisibility(false);
        mTv1.setLineVisibility(false);

        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setGradientOrientation(ShapeElement.Orientation.LEFT_TO_RIGHT);
        RgbColor[] rgbColors = new RgbColor[2];
        rgbColors[0] = RgbColor.fromArgbInt(0xFF0000E3);
        rgbColors[1] = RgbColor.fromArgbInt(0xFFDDDDFF);
        shapeElement.setRgbColors(rgbColors);
        shapeElement.setCornerRadius(20);
        mTv1.setBackground(shapeElement);
    }

    private void initTv2() {
        mTv2 = (SuperTextView) findComponentById(ResourceTable.Id_tv2);
        mTv2.setCenterText("上下渐变色");
        try {
            mTv2.setCenterTextColor(mResourceManager.getElement(ResourceTable.Color_white).getColor());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        mTv2.setLeftImageVisibility(false);
        mTv2.setRightArrowImageVisibility(false);
        mTv2.setLineVisibility(false);

        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setGradientOrientation(ShapeElement.Orientation.TOP_TO_BOTTOM);
        RgbColor[] rgbColors = new RgbColor[2];
        rgbColors[0] = RgbColor.fromArgbInt(0xFF0000E3);
        rgbColors[1] = RgbColor.fromArgbInt(0xFFDDDDFF);
        shapeElement.setRgbColors(rgbColors);
        shapeElement.setCornerRadius(20);
        mTv2.setBackground(shapeElement);
    }


    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {

        }
    }
}
