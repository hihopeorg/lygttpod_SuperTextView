package com.allen.supertextview;


import com.allen.library.SuperTextView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;

public class SuperTextAbility1 extends Ability {

    private HosTitleBar mTitleBar;
    private SuperTextView mTv1, mTv2, mTv3, mTv4, mTv5, mTv6;

    @Override
    public void onStart(Intent intent) {
        Window window = getWindow();
        window.setStatusBarColor(new Color().rgb(65, 105, 225));
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_super_text1);
        initView(intent);
        initTv1();
        initTv2();
        initTv3();
        initTv4();
        initTv5();
        initTv6();
    }

    private void initView(Intent intent) {
        mTitleBar = (HosTitleBar) findComponentById(ResourceTable.Id_titleBar);
        mTitleBar.getBackImage().setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                terminateAbility();
            }
        });
        mTitleBar.getTitleText().setText(intent.getStringParam("name"));
        mTitleBar.getRightText().setText("分享");
    }

    private void initTv1() {
        mTv1 = (SuperTextView) findComponentById(ResourceTable.Id_tv1);
        mTv1.setLeftImage(ResourceTable.Media_ic_address);
        mTv1.setLeftText("收货地址");
    }

    private void initTv2() {
        mTv2 = (SuperTextView) findComponentById(ResourceTable.Id_tv2);
        mTv2.setLeftImage(ResourceTable.Media_ic_collect);
        mTv2.setLeftText("我的收藏");
    }

    private void initTv3() {
        mTv3 = (SuperTextView) findComponentById(ResourceTable.Id_tv3);
        mTv3.setLeftImage(ResourceTable.Media_ic_album);
        mTv3.setLeftText("美食相册");
    }

    private void initTv4() {
        mTv4 = (SuperTextView) findComponentById(ResourceTable.Id_tv4);
        mTv4.setLeftImage(ResourceTable.Media_ic_integral);
        mTv4.setLeftText("积分商城");
        mTv4.setRightText("8888积分");
    }

    private void initTv5() {
        mTv5 = (SuperTextView) findComponentById(ResourceTable.Id_tv5);
        mTv5.setLeftImage(ResourceTable.Media_ic_member);
        mTv5.setLeftText("会员中心");
    }

    private void initTv6() {
        mTv6 = (SuperTextView) findComponentById(ResourceTable.Id_tv6);
        mTv6.setLeftImage(ResourceTable.Media_ic_league);
        mTv6.setLeftText("加盟合作");
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
