/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.allen.supertextview.provider;

import com.allen.supertextview.ResourceTable;
import com.allen.supertextview.bean.MainItem;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.*;

import java.util.List;

public class MainProvider extends BaseItemProvider {

    private List<MainItem> mList;
    private AbilitySlice mSlice;

    public MainProvider(List<MainItem> list, AbilitySlice slice) {
        this.mList = list;
        this.mSlice = slice;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
        Component cpt = component;
        if (cpt == null) {
            cpt = LayoutScatter.getInstance(mSlice).parse(ResourceTable.Layout_item_main, null, false);
        }
        MainItem item = mList.get(i);
        Text tv = (Text) cpt.findComponentById(ResourceTable.Id_tv);
        tv.setText(item.getName());
        return cpt;
    }
}
