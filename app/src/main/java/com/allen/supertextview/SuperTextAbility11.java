package com.allen.supertextview;


import com.allen.library.BaseTextView;
import com.allen.library.SuperTextView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.Window;

public class SuperTextAbility11 extends Ability implements Component.ClickedListener {

    private HosTitleBar mTitleBar;
    private SuperTextView mTv9;
    private BaseTextView mBaseTextView;

    private boolean baseTextViewIsClick;

    @Override
    public void onStart(Intent intent) {
        Window window = getWindow();
        window.setStatusBarColor(new Color().rgb(65, 105, 225));
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_super_text_11);
        initView(intent);
        initTv9();
    }

    private void initView(Intent intent) {

        mTitleBar = (HosTitleBar) findComponentById(ResourceTable.Id_titleBar);
        mTitleBar.getBackImage().setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                terminateAbility();
            }
        });
        mTitleBar.getTitleText().setText(intent.getStringParam("name"));
        mTitleBar.getRightImage().setPixelMap(ResourceTable.Media_ic_share);
        mBaseTextView = (BaseTextView) findComponentById(ResourceTable.Id_baseTextView);
        mBaseTextView.setClickedListener(component -> {
            baseTextViewIsClick = !baseTextViewIsClick;
            if (baseTextViewIsClick) {
                mBaseTextView.setRightImage(ResourceTable.Media_circular_check);
            } else {
                mBaseTextView.setRightImage(ResourceTable.Media_circular_not_check);
            }
            new ToastDialog(this).setText(baseTextViewIsClick + "").show();
        });
    }

    private void initTv9() {
        mTv9 = (SuperTextView) findComponentById(ResourceTable.Id_tv9);
        mTv9.setLeftText("声音");
        mTv9.setLeftImageVisibility(false);
        mTv9.setSwitchVisibility(true);
        mTv9.setSwitchIsChecked(true);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {

    }
}
