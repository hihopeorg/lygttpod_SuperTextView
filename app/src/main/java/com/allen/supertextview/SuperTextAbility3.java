package com.allen.supertextview;


import com.allen.library.SuperTextView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;
import ohos.global.resource.NotExistException;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

public class SuperTextAbility3 extends Ability {

    private HosTitleBar mTitleBar;
    private SuperTextView mTv1, mTv2, mTv3, mTv4;
    private int mTextColor;

    @Override
    public void onStart(Intent intent) {
        Window window = getWindow();
        window.setStatusBarColor(new Color().rgb(65, 105, 225));
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_super_text3);
        initView(intent);
        initTv1();
        initTv2();
        initTv3();
        initTv4();
    }

    private void initView(Intent intent) {
        mTitleBar = (HosTitleBar) findComponentById(ResourceTable.Id_titleBar);
        mTitleBar.getBackImage().setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                terminateAbility();
            }
        });
        mTitleBar.getTitleText().setText(intent.getStringParam("name"));

        ResourceManager resourceManager = getResourceManager();
        try {
            mTextColor = resourceManager.getElement(ResourceTable.Color_red).getColor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
    }

    private void initTv1() {
        mTv1 = (SuperTextView) findComponentById(ResourceTable.Id_tv1);
        mTv1.setLeftText("156****2525");
        mTv1.setCenterText("10:10");
        mTv1.setRightText("100元");
        mTv1.setRightTextColor(mTextColor);
        mTv1.setLeftImageVisibility(false);
        mTv1.setRightArrowImageVisibility(false);
    }

    private void initTv2() {
        mTv2 = (SuperTextView) findComponentById(ResourceTable.Id_tv2);
        mTv2.setLeftText("156****2525");
        mTv2.setCenterText("10:10");
        mTv2.setRightText("100元");
        mTv2.setRightTextColor(mTextColor);
        mTv2.setLeftImageVisibility(false);
        mTv2.setRightArrowImageVisibility(false);
    }

    private void initTv3() {
        mTv3 = (SuperTextView) findComponentById(ResourceTable.Id_tv3);
        mTv3.setLeftText("156****2525");
        mTv3.setCenterText("10:10");
        mTv3.setRightText("100元");
        mTv3.setRightTextColor(mTextColor);
        mTv3.setLeftImageVisibility(false);
        mTv3.setRightArrowImageVisibility(false);
    }

    private void initTv4() {
        mTv4 = (SuperTextView) findComponentById(ResourceTable.Id_tv4);
        mTv4.setLeftText("156****2525");
        mTv4.setCenterText("10:10");
        mTv4.setRightText("100元");
        mTv4.setRightTextColor(mTextColor);
        mTv4.setLeftImageVisibility(false);
        mTv4.setRightArrowImageVisibility(false);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
