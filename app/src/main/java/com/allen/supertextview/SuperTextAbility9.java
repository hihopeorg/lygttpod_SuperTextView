package com.allen.supertextview;


import com.allen.library.SuperTextView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.Window;
import ohos.global.resource.NotExistException;
import ohos.global.resource.ResourceManager;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

public class SuperTextAbility9 extends Ability implements Component.ClickedListener {

    private HosTitleBar mTitleBar;
    private SuperTextView mTv1, mTv2, mTv3, mTv4;
    private Button mBtn, mBtn2;
    private int mTextColor;

    @Override
    public void onStart(Intent intent) {
        Window window = getWindow();
        window.setStatusBarColor(new Color().rgb(65, 105, 225));
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_super_text9);
        initView(intent);
        initTv1();
        initTv2();
        initTv3();
        initTv4();
    }

    private void initView(Intent intent) {
        mTitleBar = (HosTitleBar) findComponentById(ResourceTable.Id_titleBar);
        mTitleBar.getBackImage().setClickedListener(component -> terminateAbility());
        mTitleBar.getTitleText().setText(intent.getStringParam("name"));
        mBtn = (Button) findComponentById(ResourceTable.Id_btn);
        mBtn2 = (Button) findComponentById(ResourceTable.Id_btn2);
        mBtn.setClickedListener(this);
        mBtn2.setClickedListener(this);

        ResourceManager resourceManager = getResourceManager();
        try {
            mTextColor = resourceManager.getElement(ResourceTable.Color_gray).getColor();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
    }

    private void initTv1() {
        mTv1 = (SuperTextView) findComponentById(ResourceTable.Id_tv1);
        mTv1.setLeftText("声音");
        mTv1.setLeftImageVisibility(false);
        mTv1.setSwitchVisibility(true);
        mTv1.setSwitchIsChecked(true);
        mTv1.setLineVisibility(false);
        mTv1.setBackGround(ResourceTable.Graphic_background_supertext_blue);
    }

    private void initTv2() {
        mTv2 = (SuperTextView) findComponentById(ResourceTable.Id_tv2);
        mTv2.setLeftText("账号与安全");
        mTv2.setLeftImageVisibility(false);
        mTv2.setRightText("已保护");
        mTv2.setRightImage(ResourceTable.Media_ic_defend);
        mTv2.setLineVisibility(false);
        mTv2.setBackGround(ResourceTable.Graphic_background_supertext_red);
    }

    private void initTv3() {
        mTv3 = (SuperTextView) findComponentById(ResourceTable.Id_tv3);
        mTv3.setLeftText("上圆角");
        mTv3.setLeftImageVisibility(false);
        mTv3.setLineVisibility(false);
        ShapeElement element = new ShapeElement(getContext(), ResourceTable.Graphic_background_supertext_red);
        element.setCornerRadiiArray(new float[]{
                20f, 20f,
                20f, 20f,
                0f, 0f,
                0f, 0f
        });
        mTv3.setBackground(element);
    }

    private void initTv4() {
        mTv4 = (SuperTextView) findComponentById(ResourceTable.Id_tv4);
        mTv4.setLeftText("下圆角");
        mTv4.setLeftImageVisibility(false);
        mTv4.setLineVisibility(false);
        ShapeElement element = new ShapeElement(getContext(), ResourceTable.Graphic_background_supertext_red);
        element.setCornerRadiiArray(new float[]{
                0f, 0f,
                0f, 0f,
                20f, 20f,
                20f, 20f
        });
        mTv4.setBackground(element);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_btn:
                new ToastDialog(getContext()).setText("第一个按钮点击").show();
                break;
            case ResourceTable.Id_btn2:
                new ToastDialog(getContext()).setText("第二个按钮点击").show();
                break;
        }
    }
}
