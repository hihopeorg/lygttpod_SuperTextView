package com.allen.supertextview;

import com.allen.supertextview.bean.MainItem;
import com.allen.supertextview.provider.SuperTextProvider;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;

import java.util.ArrayList;
import java.util.List;

public class SuperTextAbility10 extends Ability {

    private HosTitleBar mTitleBar;
    private ListContainer mListContainer;
    private SuperTextProvider mMainProvider;

    @Override
    public void onStart(Intent intent) {
        Window window = getWindow();
        window.setStatusBarColor(new Color().rgb(65, 105, 225));
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_super_text10);

        initView(intent);
    }

    private void initView(Intent intent) {
        mTitleBar = (HosTitleBar) findComponentById(ResourceTable.Id_titleBar);
        mTitleBar.getBackImage().setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                terminateAbility();
            }
        });
        mTitleBar.getTitleText().setText(intent.getStringParam("name"));

        List<MainItem> list = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            list.add(new MainItem("新闻标题" + i));
        }
        mListContainer = (ListContainer) findComponentById(ResourceTable.Id_listContainer);
        mMainProvider = new SuperTextProvider(list, this);
        mListContainer.setItemProvider(mMainProvider);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
