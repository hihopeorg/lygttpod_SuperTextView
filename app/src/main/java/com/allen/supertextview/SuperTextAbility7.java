package com.allen.supertextview;


import com.allen.library.SuperTextView;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;
import ohos.agp.window.service.Window;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

public class SuperTextAbility7 extends Ability implements Component.ClickedListener {

    private HosTitleBar mTitleBar;
    private SuperTextView mTv1, mTv2, mTv3, mTv4, mTv5, mTv6, mTv7, mTv8, mTv9;

    @Override
    public void onStart(Intent intent) {
        Window window = getWindow();
        window.setStatusBarColor(new Color().rgb(65, 105, 225));
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_super_text_7);
        initView(intent);
    }

    private void initView(Intent intent) {
        mTitleBar = (HosTitleBar) findComponentById(ResourceTable.Id_titleBar);
        mTitleBar.getBackImage().setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                terminateAbility();
            }
        });
        mTitleBar.getTitleText().setText(intent.getStringParam("name"));
        mTitleBar.getRightImage().setPixelMap(ResourceTable.Media_ic_share);
    }

    private void initTv6() {
        mTv6 = (SuperTextView) findComponentById(ResourceTable.Id_tv6);
        mTv6.setLeftText("100000");
        mTv6.setCenterText("666666");
        mTv6.setRightText("888888");
        try {
            mTv6.setRightTextColor(getResourceManager().getElement(ResourceTable.Color_black).getColor());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        mTv6.setLeftImageVisibility(false);
        mTv6.setRightArrowImageVisibility(false);
    }

    private void initTv7() {
        mTv7 = (SuperTextView) findComponentById(ResourceTable.Id_tv7);
        mTv7.setLeftText("点赞");
        mTv7.setCenterText("关注");
        mTv7.setRightText("粉丝");
        try {
            mTv7.setLeftTextColor(getResourceManager().getElement(ResourceTable.Color_c_9e9e9e).getColor());
            mTv7.setCenterTextColor(getResourceManager().getElement(ResourceTable.Color_c_9e9e9e).getColor());
            mTv7.setRightTextColor(getResourceManager().getElement(ResourceTable.Color_c_9e9e9e).getColor());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        mTv7.setLeftImageVisibility(false);
        mTv7.setRightArrowImageVisibility(false);
    }

    private void initTv8() {
        mTv8 = (SuperTextView) findComponentById(ResourceTable.Id_tv8);
        mTv8.setLeftText("推送通知");
        mTv8.setLeftImageVisibility(false);
        mTv8.setSwitchVisibility(true);
    }

    private void initTv9() {
        mTv9 = (SuperTextView) findComponentById(ResourceTable.Id_tv9);
        mTv9.setLeftText("声音");
        mTv9.setLeftImageVisibility(false);
        mTv9.setSwitchVisibility(true);
        mTv9.setSwitchIsChecked(true);
    }

    private void initTv1() {
        mTv1 = (SuperTextView) findComponentById(ResourceTable.Id_tv1);
        mTv1.setLeftImage(ResourceTable.Media_ic_head);
        mTv1.setLeftText("账号管理");
    }

    private void initTv2() {
        mTv2 = (SuperTextView) findComponentById(ResourceTable.Id_tv2);
        mTv2.setLeftImageVisibility(false);
        mTv2.setLeftText("通知设置");
    }

    private void initTv3() {
        mTv3 = (SuperTextView) findComponentById(ResourceTable.Id_tv3);
        mTv3.setLeftImageVisibility(false);
        mTv3.setLeftText("账号与安全");
        mTv3.setRightText("已保护");
        mTv3.setRightImage(ResourceTable.Media_ic_defend);
    }

    private void initTv4() {
        mTv4 = (SuperTextView) findComponentById(ResourceTable.Id_tv4);
        mTv4.setLeftImageVisibility(false);
        mTv4.setLeftText("意见反馈");
    }

    private void initTv5() {
        mTv5 = (SuperTextView) findComponentById(ResourceTable.Id_tv5);
        mTv5.setLeftImageVisibility(false);
        mTv5.setLeftText("关于微博");
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {

    }
}
