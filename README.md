# SuperTextView

**本项目是基于开源项目SuperTextView进行ohos化的移植和开发的，可以通过项目标签以及github地址（https://github.com/lygttpod/SuperTextView ）追踪到原项目版本**

#### 项目介绍

- 项目名称：一个超级文本视图
- 所属系列：ohos的第三方组件适配移植
- 功能：SuperTextView是一个功能强大的View，可以满足日常大部分布局样式；SuperButton拥有shape文件的大部分属性，从此写shape属性变得非常简单
- 项目移植状态：完成
- 调用差异：无
- 项目作者和维护人：hihope
- 联系方式：hihope@hoperun.com
- 原项目Doc地址：https://github.com/lygttpod/SuperTextView
- 原项目基线版本：v2.4.5 , sha1:218e75e640489ecd21643e35c505a89b84fde90a
- 编程语言：Java 
- 外部库依赖：无

#### 效果展示

<img src="preview/preview.gif"/>

#### 安装教程

方法1.

1. 编译har包SuperTextView.har。
2. 启动 DevEco Studio，将编译的har包，导入工程目录“entry->libs”下。
3. 在moudle级别下的build.gradle文件中添加依赖，在dependences标签中增加对libs目录下har包的引用。

```
dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar', '*.har'])
	……
}
```

4. 在导入的har包上点击右键，选择“Add as Library”对包进行引用，选择需要引用的模块，并点击“OK”即引用成功。

方法2.

1. 在工程的build.gradle的allprojects中，添加HAR所在的Maven仓地址

```
repositories {
    maven {
        url 'http://106.15.92.248:8081/repository/Releases/' 
    }
}
```

2. 在应用模块的build.gradle的dependencies闭包中，添加如下代码:

```
dependencies {
    implementation 'com.github.lygttpod.ohos:SuperTextView:1.0.1'
}
```

#### 使用说明

1. 圆角带边框矩形

```
    <com.allen.library.SuperButton
            ohos:height="match_content"
            ohos:width="match_content"
            ohos:left_margin="5vp"
            app:bg_color="0x20000000"
            app:bottomLeft_conner="15f"
            app:bottomRight_conner="15f"
            app:bottom_padding="70"
            app:isTextCenter="false"
            app:left_padding="20"
            app:right_padding="20"
            app:shape="0"
            app:stroke_color="0xFFFF4081"
            app:stroke_width="2"
            app:title_color="$color:black"
            app:title_size="13"
            app:title_text="圆角边框"
            app:topLeft_conner="15f"
            app:topRight_conner="15f"
        app:top_padding="70"
         />
```

 

2. 单独设置圆角
```
    //上圆角
    <com.allen.library.SuperButton
              ohos:height="match_content"
              ohos:width="match_content"
              ohos:left_margin="5vp"
              app:bg_color="0x20000000"
              app:bottomLeft_conner="0f"
              app:bottomRight_conner="0f"
              app:bottom_padding="45"
              app:isTextCenter="false"
              app:left_padding="80"
              app:right_padding="80"
              app:shape="0"
              app:stroke_color="0xFF3F51B5"
              app:stroke_width="2"
              app:title_color="$color:black"
              app:title_size="13"
              app:title_text="上圆角"
              app:topLeft_conner="50f"
              app:topRight_conner="50f"
              app:top_padding="45"
              />
```

```
    //斜对角圆角
    <com.allen.library.SuperButton
              ohos:height="match_content"
              ohos:width="match_content"
              ohos:left_margin="5vp"
              app:bg_color="0x20000000"
              app:bottomLeft_conner="50f"
              app:bottomRight_conner="0f"
              app:bottom_padding="45"
              app:isTextCenter="false"
              app:left_padding="80"
              app:right_padding="80"
              app:shape="0"
              app:stroke_color="0xFF3F51B5"
              app:stroke_width="2"
              app:title_color="$color:black"
              app:title_size="13"
          app:title_text="斜对角圆角"
              app:topLeft_conner="0f"
              app:topRight_conner="50f"
              app:top_padding="45"
              />
```

3. 椭圆

```
    <com.allen.library.SuperButton
             ohos:height="match_content"
             ohos:width="match_content"
             ohos:left_margin="5vp"
             app:bg_color="0x20000000"
             app:bottomLeft_conner="50f"
             app:bottomRight_conner="0f"
             app:bottom_padding="45"
             app:isTextCenter="false"
             app:left_padding="80"
             app:right_padding="80"
             app:shape="1"
             app:stroke_color="0xFF3F51B5"
             app:stroke_width="2"
             app:title_color="$color:black"
             app:title_size="13"
             app:title_text="椭圆"
             app:topLeft_conner="0f"
             app:topRight_conner="0f"
         app:top_padding="45"
          />
```

 

4. 圆形

```
    <com.allen.library.SuperButton
             ohos:height="50vp"
             ohos:width="50vp"
             app:bg_color="0xFFFF4081"
             app:bottomLeft_conner="0f"
             app:bottomRight_conner="0f"
             app:bottom_padding="0"
             app:isTextCenter="true"
             app:left_padding="0"
             app:right_padding="0"
             app:shape="1"
             app:stroke_color="0xFF3F51B5"
             app:stroke_width="2"
             app:title_color="$color:black"
             app:title_size="13"
             app:title_text="圆形"
             app:topLeft_conner="0f"
             app:topRight_conner="0f"
         app:top_padding="0"
          />
```

 

5. 渐变色

```
ShapeElement shapeElement = new ShapeElement();
        shapeElement.setGradientOrientation(ShapeElement.Orientation.LEFT_TO_RIGHT);
        RgbColor[] rgbColors = new RgbColor[2];
        rgbColors[0] = RgbColor.fromArgbInt(0xFF0000E3);
        rgbColors[1] = RgbColor.fromArgbInt(0xFFDDDDFF);
        shapeElement.setRgbColors(rgbColors);
        shapeElement.setCornerRadius(20);
        mTv1.setBackground(shapeElement);
```

6. 滑动按钮

```
   <com.allen.library.SuperTextView
           ohos:id="$+id:tv1"
           ohos:height="match_content"
           ohos:width="match_content"/>
   
   mTv1 = (SuperTextView) findComponentById(ResourceTable.Id_tv1);
           mTv1.setLeftText("系统默认");
           mTv1.setLeftImageVisibility(false);
           mTv1.setSwitchVisibility(true);
```



#### 版本迭代

- v1.0.1

#### 版权和许可信息
- Apache Licence
