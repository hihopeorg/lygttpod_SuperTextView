package com.allen.library;

import ohos.agp.components.*;
import ohos.app.Context;

public class CommonTextView extends ComponentContainer {

    public CommonTextView(Context context) {
        super(context);
    }

    public CommonTextView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        initView(context, attrSet);
    }

    public CommonTextView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initView(context, attrSet);
    }

    private void initView(Context context, AttrSet attrSet) {

        Component component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_layout_common_textview, null, false);
        Image backImage = (Image) component.findComponentById(ResourceTable.Id_backImage);
        Text titleText = (Text) component.findComponentById(ResourceTable.Id_titleText);
        Text leftText = (Text) component.findComponentById(ResourceTable.Id_leftText);
        Text rightText = (Text) component.findComponentById(ResourceTable.Id_rightText);

        addComponent(component);

        component.setBackground(attrSet.getAttr("bg_color").get().getElement());

        backImage.setImageElement(attrSet.getAttr("back_image").get().getElement());

        leftText.setText(attrSet.getAttr("left_text").get().getStringValue());

        titleText.setText(attrSet.getAttr("title_text").get().getStringValue());
        titleText.setTextColor(attrSet.getAttr("title_color").get().getColorValue());
        titleText.setTextSize(attrSet.getAttr("title_size").get().getIntegerValue(), Text.TextSizeType.FP);

        rightText.setText(attrSet.getAttr("right_text").get().getStringValue());
        rightText.setTextColor(attrSet.getAttr("right_color").get().getColorValue());
        rightText.setTextSize(attrSet.getAttr("right_size").get().getIntegerValue(), Text.TextSizeType.FP);
    }
}
