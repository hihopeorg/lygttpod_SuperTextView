package com.allen.library;

import ohos.agp.components.*;
import ohos.app.Context;

public class BaseTextView extends ComponentContainer {
    private Text titleText, bottomText;
    private Image rightImage;

    public BaseTextView(Context context) {
        super(context);
    }

    public BaseTextView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        initView(context, attrSet);
    }

    public BaseTextView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initView(context, attrSet);
    }

    private void initView(Context context, AttrSet attrSet) {

        Component component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_layout_super_text, null, false);
        Image backImage = (Image) component.findComponentById(ResourceTable.Id_backImage);
        rightImage = (Image) component.findComponentById(ResourceTable.Id_rightImage);
        titleText = (Text) component.findComponentById(ResourceTable.Id_titleText);
        bottomText = (Text) component.findComponentById(ResourceTable.Id_bottomText);
        Text bottomRightText = (Text) component.findComponentById(ResourceTable.Id_bottomRightText);
        Text rightText = (Text) component.findComponentById(ResourceTable.Id_rightText);

        addComponent(component);

        component.setBackground(attrSet.getAttr("bg_color").get().getElement());

        backImage.setImageElement(attrSet.getAttr("back_image").get().getElement());

        titleText.setText(attrSet.getAttr("title_text").get().getStringValue());
        titleText.setTextColor(attrSet.getAttr("title_color").get().getColorValue());
        titleText.setTextSize(attrSet.getAttr("title_size").get().getIntegerValue(), Text.TextSizeType.FP);

        bottomText.setText(attrSet.getAttr("bottom_text").get().getStringValue());
        bottomText.setTextColor(attrSet.getAttr("bottomText_color").get().getColorValue());
        if (!attrSet.getAttr("isShowBottomText").get().getBoolValue()) {
            bottomText.setVisibility(HIDE);
        }

        bottomRightText.setText(attrSet.getAttr("bottomRight_text").get().getStringValue());

        rightText.setText(attrSet.getAttr("right_text").get().getStringValue());
        rightText.setTextColor(attrSet.getAttr("right_color").get().getColorValue());
        rightText.setTextSize(attrSet.getAttr("right_size").get().getIntegerValue(), Text.TextSizeType.FP);
        if (!attrSet.getAttr("isShowRightText").get().getBoolValue()) {
            rightText.setVisibility(HIDE);
        }

        rightImage.setImageElement(attrSet.getAttr("right_image").get().getElement());
        if (!attrSet.getAttr("isShowRightImage").get().getBoolValue()) {
            rightImage.setVisibility(HIDE);
        }
    }

    public void setTitleText(String str) {
        titleText.setText(str);
    }

    public void setRightImage(int pixel) {
        rightImage.setPixelMap(pixel);
    }

    public Image getRightImage() {
        return rightImage;
    }

    public Text getBottomText() {
        return bottomText;
    }
}
