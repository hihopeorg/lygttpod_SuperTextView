package com.allen.library;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.StateElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

public class SuperTextView extends DirectionalLayout {

    private DependentLayout mDependentLayout;
    private DirectionalLayout mSwitchComponent, mRightComponent;
    private Component mLineComponent;
    private Text mLeftText, mCenterText, mRightText;
    private Image mLeftImage, mRightImage, mRightArrowImage;
    private Switch mSwitch;
    private DependentLayout centerDependentLayout;

    private final int LEFT_IMAGE = ResourceTable.Media_head_default;
    private final int RIGHT_IMAGE = ResourceTable.Media_ic_right;

    private final int LEFT_TEXT_SIZE = 15;
    private final int CENTER_TEXT_SIZE = 15;
    private final int RIGHT_TEXT_SIZE = 15;

    private final int LINE_HEIGHT = 2;

    private final int LEFT_IMAGE_RIGHT_MARGIN = 35;
    private final int RIGHT_IMAGE_RIGHT_MARGIN = 20;
    private final int LEFT_PADDING = 50;
    private final int RIGHT_PADDING = 50;
    private final int TOP_PADDING = 40;
    private final int BOTTOM_PADDING = 40;

    public SuperTextView(Context context) {
        super(context);
        initView();
    }

    public SuperTextView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        initView();
    }

    public SuperTextView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initView();
    }

    private void initView() {
        setOrientation(Component.VERTICAL);
        setWidth(LayoutConfig.MATCH_PARENT);
        setHeight(LayoutConfig.MATCH_CONTENT);
        mDependentLayout = new DependentLayout(getContext());
        mDependentLayout.setPaddingLeft(LEFT_PADDING);
        mDependentLayout.setPaddingRight(RIGHT_PADDING);
        mDependentLayout.setWidth(LayoutConfig.MATCH_PARENT);
        mDependentLayout.setHeight(165);
        initLeftComponent();
        initCenterText();
        initRightComponent();
        initSwitch();
        addComponent(mDependentLayout);
        initLine();
    }

    private void initLeftComponent() {
        DirectionalLayout directionalLayout = new DirectionalLayout(getContext());
        directionalLayout.setOrientation(Component.HORIZONTAL);
        directionalLayout.setHeight(LayoutConfig.MATCH_PARENT);
        directionalLayout.setWidth(LayoutConfig.MATCH_CONTENT);
        directionalLayout.setAlignment(LayoutAlignment.VERTICAL_CENTER);
        initLeftImage(directionalLayout);
        initLeftText(directionalLayout);
        mDependentLayout.addComponent(directionalLayout);
    }

    private void initLeftImage(DirectionalLayout directionalLayout) {
        mLeftImage = new Image(getContext());
        mLeftImage.setPixelMap(LEFT_IMAGE);
        mLeftImage.setMarginRight(LEFT_IMAGE_RIGHT_MARGIN);
        directionalLayout.addComponent(mLeftImage);
    }

    private void initLeftText(DirectionalLayout directionalLayout) {
        mLeftText = new Text(getContext());
        mLeftText.setTextSize(LEFT_TEXT_SIZE, Text.TextSizeType.FP);
        directionalLayout.addComponent(mLeftText);
    }

    private void initCenterText() {
        centerDependentLayout = new DependentLayout(getContext());
        centerDependentLayout.setHeight(DependentLayout.LayoutConfig.MATCH_PARENT);
        centerDependentLayout.setWidth(DependentLayout.LayoutConfig.MATCH_PARENT);
        centerDependentLayout.setAlignment(LayoutAlignment.CENTER);
        mCenterText = new Text(getContext());
        mCenterText.setTextSize(CENTER_TEXT_SIZE, Text.TextSizeType.FP);
        centerDependentLayout.addComponent(mCenterText);
        mDependentLayout.addComponent(centerDependentLayout);
    }

    private void initRightComponent() {
        mRightComponent = new DirectionalLayout(getContext());
        mRightComponent.setWidth(DependentLayout.LayoutConfig.MATCH_PARENT);
        mRightComponent.setHeight(DependentLayout.LayoutConfig.MATCH_PARENT);
        mRightComponent.setOrientation(Component.HORIZONTAL);
        mRightComponent.setAlignment(LayoutAlignment.RIGHT | LayoutAlignment.VERTICAL_CENTER);
        initRightImage(mRightComponent);
        initRightText(mRightComponent);
        initRightArrowImage(mRightComponent);
        mDependentLayout.addComponent(mRightComponent);
    }

    private void initRightImage(DirectionalLayout directionalLayout) {
        mRightImage = new Image(getContext());
        mRightImage.setMarginRight(RIGHT_IMAGE_RIGHT_MARGIN);
        directionalLayout.addComponent(mRightImage);
    }

    private void initRightText(DirectionalLayout directionalLayout) {
        mRightText = new Text(getContext());
        mRightText.setTextSize(RIGHT_TEXT_SIZE, Text.TextSizeType.FP);
        mRightText.setTextColor(Color.GRAY);
        directionalLayout.addComponent(mRightText);
    }

    private void initRightArrowImage(DirectionalLayout directionalLayout) {
        mRightArrowImage = new Image(getContext());
        mRightArrowImage.setPixelMap(RIGHT_IMAGE);
        directionalLayout.addComponent(mRightArrowImage);
    }

    private void initSwitch() {
        mSwitchComponent = new DirectionalLayout(getContext());
        mSwitchComponent.setWidth(DependentLayout.LayoutConfig.MATCH_PARENT);
        mSwitchComponent.setHeight(DependentLayout.LayoutConfig.MATCH_PARENT);
        mSwitchComponent.setOrientation(Component.HORIZONTAL);
        mSwitchComponent.setAlignment(LayoutAlignment.RIGHT | LayoutAlignment.VERTICAL_CENTER);
        mSwitch = new Switch(getContext());
        mSwitch.setWidth(120);
        mSwitch.setHeight(60);
        mSwitch.setMarginRight(RIGHT_IMAGE_RIGHT_MARGIN);
        mSwitch.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {

            @Override
            public void onCheckedChanged(AbsButton button, boolean isChecked) {
                new ToastDialog(getContext()).setText(isChecked + "").show();
            }
        });

        ShapeElement elementThumbOn = new ShapeElement();
        elementThumbOn.setShape(ShapeElement.OVAL);
        elementThumbOn.setRgbColor(RgbColor.fromArgbInt(0xFF1E90FF));
        elementThumbOn.setCornerRadius(50);

        ShapeElement elementThumbOff = new ShapeElement();
        elementThumbOff.setShape(ShapeElement.OVAL);
        elementThumbOff.setRgbColor(RgbColor.fromArgbInt(0xFFFFFFFF));
        elementThumbOff.setCornerRadius(50);

        ShapeElement elementTrackOn = new ShapeElement();
        elementTrackOn.setShape(ShapeElement.RECTANGLE);
        elementTrackOn.setRgbColor(RgbColor.fromArgbInt(0xFF87CEFA));
        elementTrackOn.setCornerRadius(50);

        ShapeElement elementTrackOff = new ShapeElement();
        elementTrackOff.setShape(ShapeElement.RECTANGLE);
        elementTrackOff.setRgbColor(RgbColor.fromArgbInt(0xFF808080));
        elementTrackOff.setCornerRadius(50);
        mSwitch.setTrackElement(trackElementInit(elementTrackOn, elementTrackOff));
        mSwitch.setThumbElement(thumbElementInit(elementThumbOn, elementThumbOff));

        mSwitchComponent.addComponent(mSwitch);
        mDependentLayout.addComponent(mSwitchComponent);
        mSwitchComponent.setVisibility(Component.HIDE);
    }

    private void initLine() {
        mLineComponent = new Component(getContext());
        ShapeElement element = new ShapeElement(getContext(), ResourceTable.Graphic_background_ability_super_text);
        mLineComponent.setBackground(element);
        mLineComponent.setHeight(LINE_HEIGHT);
        addComponent(mLineComponent);
    }

    public void setBackGround(int graphic) {
        ShapeElement element = new ShapeElement(getContext(), graphic);
        mDependentLayout.setBackground(element);
    }

    public void setLeftImage(int img) {
        mLeftImage.setPixelMap(img);
    }

    public void setLeftImageVisibility(boolean isVisibility) {
        if (isVisibility) {
            mLeftImage.setVisibility(Component.VERTICAL);
        } else {
            mLeftImage.setVisibility(Component.HIDE);
        }
    }

    public void setLeftText(String name) {
        mLeftText.setText(name);
    }

    public void setLeftTextColor(int color) {
        mLeftText.setTextColor(new Color(color));
    }

    public void setCenterText(String name) {
        mCenterText.setText(name);
    }

    public void setCenterTextColor(int color) {
        mCenterText.setTextColor(new Color(color));
    }

    public void setRightText(String name) {
        mRightText.setText(name);
    }

    public void setRightTextColor(int color) {
        mRightText.setTextColor(new Color(color));
    }

    public int getRightTextColor() {
        return mRightText.getTextColor().getValue();
    }

    public void setRightImage(int img) {
        mRightImage.setPixelMap(img);
    }

    public void setRightArrowImageVisibility(boolean isVisibility) {
        if (isVisibility) {
            mRightArrowImage.setVisibility(Component.VERTICAL);
        } else {
            mRightArrowImage.setVisibility(Component.HIDE);
        }
    }

    public void setSwitchVisibility(boolean isVisibility) {
        if (isVisibility) {
            mRightComponent.setVisibility(Component.HIDE);
            mSwitchComponent.setVisibility(Component.VISIBLE);
        } else {
            mRightComponent.setVisibility(Component.VISIBLE);
            mSwitchComponent.setVisibility(Component.HIDE);
        }
    }

    public void setSwitchIsChecked(boolean isChecked) {
        mSwitch.setChecked(isChecked);
    }

    public void setLineVisibility(boolean isVisibility) {
        if (isVisibility) {
            mLineComponent.setVisibility(Component.VISIBLE);
        } else {
            mLineComponent.setVisibility(Component.HIDE);
        }
    }

    private StateElement trackElementInit(ShapeElement on, ShapeElement off) {
        StateElement trackElement = new StateElement();
        trackElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, on);
        trackElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, off);
        return trackElement;
    }

    private StateElement thumbElementInit(ShapeElement on, ShapeElement off) {
        StateElement thumbElement = new StateElement();
        thumbElement.addState(new int[]{ComponentState.COMPONENT_STATE_CHECKED}, on);
        thumbElement.addState(new int[]{ComponentState.COMPONENT_STATE_EMPTY}, off);
        return thumbElement;
    }

    public DependentLayout getCenterDependentLayout() {
        return centerDependentLayout;
    }

    public Switch getSwitch() {
        return mSwitch;
    }

    public Image getLeftImage() {
        return mLeftImage;
    }

    public Image getRightImage() {
        return mRightImage;
    }

    public Image getRightArrowImage() {
        return mRightArrowImage;
    }

    public Text getLeftText() {
        return mLeftText;
    }

    public Text getRightText() {
        return mRightText;
    }

    public Text getCenterText() {
        return mCenterText;
    }

    public Component getLineComponent() {
        return mLineComponent;
    }
}
