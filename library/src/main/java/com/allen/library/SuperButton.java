package com.allen.library;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.LayoutAlignment;
import ohos.app.Context;

public class SuperButton extends ComponentContainer {

    private Text titleText;

    public SuperButton(Context context) {
        super(context);
    }

    public SuperButton(Context context, AttrSet attrSet) {
        super(context, attrSet);
        initView(context, attrSet);
    }

    public SuperButton(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initView(context, attrSet);
    }

    private void initView(Context context, AttrSet attrSet) {

        Component component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_layout_super_button, null, false);
        titleText = (Text) component.findComponentById(ResourceTable.Id_titleText);
        DirectionalLayout parentV = (DirectionalLayout) component.findComponentById(ResourceTable.Id_parentV);
        addComponent(component);
        component.setWidth(DirectionalLayout.LayoutConfig.MATCH_PARENT);
        component.setHeight(DirectionalLayout.LayoutConfig.MATCH_PARENT);
        component.setPaddingLeft(attrSet.getAttr("left_padding").get().getIntegerValue());
        component.setPaddingRight(attrSet.getAttr("right_padding").get().getIntegerValue());
        component.setPaddingTop(attrSet.getAttr("top_padding").get().getIntegerValue());
        component.setPaddingBottom(attrSet.getAttr("bottom_padding").get().getIntegerValue());

        titleText.setText(attrSet.getAttr("title_text").get().getStringValue());
        titleText.setTextColor(attrSet.getAttr("title_color").get().getColorValue());
        titleText.setTextSize(attrSet.getAttr("title_size").get().getIntegerValue(), Text.TextSizeType.FP);

        Float topLeft_conner = attrSet.getAttr("topLeft_conner").get().getFloatValue();
        Float topRight_conner = attrSet.getAttr("topRight_conner").get().getFloatValue();
        Float bottomLeft_conner = attrSet.getAttr("bottomLeft_conner").get().getFloatValue();
        Float bottomRight_conner = attrSet.getAttr("bottomRight_conner").get().getFloatValue();
        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(attrSet.getAttr("bg_color").get().getIntegerValue()));
        element.setStroke(attrSet.getAttr("stroke_width").get().getIntegerValue()
                , RgbColor.fromArgbInt(attrSet.getAttr("stroke_color").get().getIntegerValue()));
        element.setCornerRadiiArray(new float[]{
                topLeft_conner, topLeft_conner,
                topRight_conner, topRight_conner,
                bottomRight_conner, bottomRight_conner,
                bottomLeft_conner, bottomLeft_conner
        });
        element.setShape(attrSet.getAttr("shape").get().getIntegerValue());
        parentV.setBackground(element);
        if (attrSet.getAttr("isTextCenter").get().getBoolValue()) {
            parentV.setAlignment(LayoutAlignment.CENTER);
        }
    }

    public void setTitleText(String str) {
        titleText.setText(str);
    }

}
